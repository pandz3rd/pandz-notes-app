const addBtn = document.getElementById('add')
const notesText = JSON.parse(localStorage.getItem('notes'))

console.log('All: ', notesText)

const addNewNote = (noteTxt = '') => {
  const newNote = document.createElement('div')

  newNote.classList.add('notes')

  newNote.innerHTML = `
    <div class="tools">
      <button class="edit">
        <i class="fas fa-edit"></i>
      </button>
      <button class="delete">
        <i class="fas fa-trash-alt"></i>
      </button>
    </div>
    <div class="content hidden">

    </div>
    <textarea></textarea>
  `

  const editBtn = newNote.querySelector('.edit')
  const deleteBtn = newNote.querySelector('.delete')

  const main = newNote.querySelector('.content')
  const textArea = newNote.querySelector('textarea')

  main.innerHTML = marked(noteTxt)
  textArea.value = noteTxt

  editBtn.addEventListener('click', () => {
    main.classList.toggle('hidden')
    textArea.classList.toggle('hidden')
  })

  textArea.addEventListener('input', (e) => {
    const { value } = e.target
    main.innerHTML = marked(value)

    updateLocalstorage()
  })

  deleteBtn.addEventListener('click', ()=> {
    newNote.remove()
    updateLocalstorage()
  })

  document.body.appendChild(newNote)
}

if (notesText) {
  notesText.forEach(note => {
    addNewNote(note)
  })
}

addBtn.addEventListener('click', ()=> {
  addNewNote()
})

const updateLocalstorage = () => {
  const allNotes = document.querySelectorAll('textarea')
  const notes = []

  allNotes.forEach(note => {
    notes.push(note.value)
  })

  localStorage.setItem('notes', JSON.stringify(notes))
}
